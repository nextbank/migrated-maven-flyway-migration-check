package com.nextbank;


import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.codehaus.plexus.util.StringUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Mojo(name = "validate-flyway-migration", defaultPhase = LifecyclePhase.VALIDATE)
public class FlywayMigrationValidatorMojo
  extends AbstractMojo {

  @Parameter(property = "basedir", required = true, readonly = true)
  private String basedir;
  @Parameter(property = "validate-mybatis-migration.migration-path", required = true)
  private String migrationPath;
  @Parameter(property = "validate-mybatis-migration.regex", required = true, defaultValue = "V(\\d+\\.\\d+\\.\\d+\\.\\d+(_\\d+)?)\\w+\\.sql")
  private String regex;

  public void execute()
    throws MojoExecutionException {
    Log log = getLog();

    String pathname = basedir + migrationPath;
    pathname = StringUtils.escape(pathname);

    File dirFile = new File(pathname);
    File[] files = dirFile.listFiles();
    if (files == null) {
      String message = "No files in :" + pathname;
      log.error(message);
      throw new MojoExecutionException(message);
    }
    List<String> filesNames = new ArrayList<>();
    Set<String> migrationNumbers = new HashSet<>();

    for (File file : files) {
      filesNames.add(file.getName());
    }

    Pattern compile = Pattern.compile(regex);


    for (String fileName : filesNames) {
      log.info("Processing " + fileName);
      Matcher matcher = compile.matcher(fileName);
      if (matcher.matches()) {
        String group = matcher.group(1);
        String optionalGroup = "";
        if (matcher.groupCount() == 2) {
          optionalGroup = matcher.group(2);
        }

        if (!migrationNumbers.add(group + optionalGroup)) {
          String message = "Duplication in " + fileName;
          log.error(message);
          throw new MojoExecutionException(message);
        }
      } else {
        String message = "No match found: " + fileName;
        log.error(message);
        throw new MojoExecutionException(message);

      }
    }

    log.info("Flyway migration validation passed.");
  }
}
